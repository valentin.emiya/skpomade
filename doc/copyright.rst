Credits
#######

Copyright(c) 2019-2020
----------------------

* Laboratoire d'Informatique et Systèmes <http://www.lis-lab.fr/>
* Université d'Aix-Marseille <http://www.univ-amu.fr/>
* Centre National de la Recherche Scientifique <http://www.cnrs.fr/>
* Université de Toulon <http://www.univ-tln.fr/>

Contributors
------------

* Valentin Emiya <firstname.lastname_AT_lis-lab.fr>

This package has been created thanks to the joint work with Florent Jaillet
and Ronan Hamon on other packages.

Description
-----------

`skpomade` is a Python implementation of algorithms from
paper *Finding Structure with Randomness: Probabilistic Algorithms for
Constructing Approximate Matrix Decompositions*, by N. Halko, P. G.
Martinsson and J. A. Tropp, SIAM review, 53 (2), 2011, https://arxiv.org/abs/0909.4061.


Version
-------

* skpomade version = 0.1.4

Licence
-------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

