References
==========

    :Release: |release|
    :Date: |today|

skpomade\.factorization_construction module
-------------------------------------------

.. automodule:: skpomade.factorization_construction
    :members:
    :undoc-members:
    :show-inheritance:

skpomade\.range_approximation module
------------------------------------

.. automodule:: skpomade.range_approximation
    :members:
    :undoc-members:
    :show-inheritance:

skpomade\.utils module
----------------------

.. automodule:: skpomade.utils
    :members:
    :undoc-members:
    :show-inheritance:
