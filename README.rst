skpomade
========

skpomade: PrObabilistic MAtrix DEcompositions from Halko et al., 2011

Python implementation of algorithms from paper *Finding Structure
with Randomness: Probabilistic Algorithms for Constructing Approximate Matrix
Decompositions*, by N. Halko, P. G. Martinsson and J. A. Tropp, SIAM review,
53 (2), 2011.


Install
-------

Install the current release with ``pip``::

    pip install skpomade

For additional details, see doc/install.rst.

Usage
-----

See the `documentation <http://valentin.emiya.pages.lis-lab.fr/skpomade/>`_.

Bugs
----

Please report any bugs that you find through the `skpomade GitLab project
<https://gitlab.lis-lab.fr/valentin.emiya/skpomade/issues>`_.

You can also fork the repository and create a merge request.

Source code
-----------

The source code of skpomade is available via its `GitLab project
<https://gitlab.lis-lab.fr/valentin.emiya/skpomade>`_.

You can clone the git repository of the project using the command::

    git clone git@gitlab.lis-lab.fr:valentin.emiya/skpomade.git

Copyright © 2019-2020
---------------------

* `Laboratoire d'Informatique et Systèmes <http://www.lis-lab.fr/>`_
* `Université d'Aix-Marseille <http://www.univ-amu.fr/>`_
* `Centre National de la Recherche Scientifique <http://www.cnrs.fr/>`_
* `Université de Toulon <http://www.univ-tln.fr/>`_

Contributors
------------

* `Valentin Emiya <mailto:valentin.emiya@lis-lab.fr>`_

License
-------

Released under the GNU General Public License version 3 or later
(see `LICENSE.txt`).
